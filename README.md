Welcome to Cat Weather for Poly phones UCS 4.0+
=================

TheCatAPI for Poly phones 480x256

This turned into a tech demo of
- service workers
- web workers
- parallel API calls
- todo: WASM

# Todo:
- Fix service worker caching by:
  - Using a CacheFirst strategy
  - Looping a fetch waiter, 5s w inc backoff
  - Reload clients if data is different
  - Present degraded version on 500 errors (Like NOAA quota exeeded)

# Tl;Dr:

To get NYC weather use: `https://friggin-cat-weather.glitch.me/?lat=40.73&long=-73.93`


## Original intent
Polycom phones have a browser... effectively about as capable as the Google-HTC G1.

This is desined to show on the idle browser for the "landline" phone.
It refreshes every hour. images are optimized for the phone if needed.
Weather data is cached as they tell us to.

## Development
- I have a Cat API key. Add it to .env as `CAT_API_KEY`
  - https://thecatapi.com/
- I have a NOAA Web Service API Token, request one from
  - https://www.ncdc.noaa.gov/cdo-web/token
  - Using a key makes responses up to ~20 seconds faster... 
    and will break less after around 42 calls an hour

## Bugs

* Using query parameters breaks the Glitch loading page protection
  * Because prefetch (priority cache) is hard coded
  * Solution is to copy all good documents to prefetch?
  * Or not use query parameters, but indexeddb / local storage/ hash instead.

# Design
- ServiceWorker
  - Loads CacheFirst, new version will be used next load
  - Uses status code to determine whether to cache response for next load.
  - No indication that the cache was used is given or not used is given

### Features I want to add

* Handle the overflow of text better for phones rotating
* full screen cat pictures
* Accept and Email/Push Notify/Display, public keys
* App Manifest to enable background features (~ once per day)
* Local cat photo history
* Graphical settings menu
* Customize to Black, ability to disable everything.

Your Project
------------

On the front-end,
- 'https://thecatapi.com/'
- edit `public/client.js`, `public/style.css` and `views/index.html`
- drag in `assets`, like images or music, to add them to your project

On the back-end,
- your app starts at `server.js`
- add frameworks and packages in `package.json`
- safely store app secrets in `.env` (nobody can see this but you and people you invite)


Made by [Ray Foss](https://vblip.com/)
-------------------

\ ゜o゜)ノ
