Not supported in Firefox 2021-09

```
const readable = response.body
const transformed = readable.pipeThrough(new sedDoc())
const initR = {
  status: response.status,
  statusText: response.statusText,
  headers: Object.fromEntries(headers)
}
const myResponse = new Response(transformed, initR)
try {
  cache.put(event.request, myResponse.clone())
} catch (e) {
  console.warn('Could not save doc to cache.', e)
}
return myResponse
```