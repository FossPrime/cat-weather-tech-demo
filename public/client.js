/*function ajax_get(url, callback) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      // console.log('responseText:' + xmlhttp.responseText);
      try {
        var data = JSON.parse(xmlhttp.responseText);
      } catch (err) {
        // console.log(err.message + " in " + xmlhttp.responseText);
        return;
      }
      callback(data);
    }
  };

  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

ajax_get('https://api.thecatapi.com/v1/images/search?size=full', function(data) {
  //document.getElementById("id").innerHTML = data[0]["id"];
  //document.getElementById("url").innerHTML = data[0]["url"];

  var html = '<img src="' + data[0]["url"] + '">';
  document.getElementById("image").innerHTML = html;
});
*/
import _ from './utils.js';
import registerQuotes from './quotes.js'

const sm = {
  d: document,
  swReg: {} // Registered Service Worker
}

// SW LifeCycle: https://pastebin.com/2eR0tTRT
async function main () {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js')
    .then(function (reg) {
      sm.swReg = reg.installing || reg.waiting || reg.active
      if (sm.swReg.state === 'activated') {
        logMyHeaders() // makes a cache query for gdamn headers
      } else {
        sm.swReg.addEventListener('statechange', logMyHeadersSubscriber)
      }
    })
  }
  
  window.addEventListener('DOMContentLoaded', registerUIHandlers)
  
}

function registerUIHandlers(event) {
  registerQuotes('#quote')
  
  const shortDateEl = sm.d.querySelector('#shortDate')
  shortDateEl.addEventListener('click', registerDateClickHandler)
}

// Delete-ish service worker and restart!
const registerDateClickHandler = async (event) => {
  const registrations = await navigator.serviceWorker.getRegistrations()
  if(registrations[0]) {
    await registrations[0].unregister()
  }
  
  const cacheKeys = await caches.keys()
  cacheKeys.forEach((c) => { caches.delete(c) })
  window.location.reload()
}


const logMyHeadersSubscriber = function (e) {
  if (e.target.state === 'activated') {
    logMyHeaders()
    // sm.swReg.removeEventListener('statechange', logMyHeadersSubscriber)
  } else { // About redundant: https://stackoverflow.com/a/59667772/370238
    console.log(`logMyHeadersSubscriber ${e.target.state}!`)
  }
}
// Finds all headers of this document from cache.
const logMyHeaders = async () => {
  const ourHeader = 'rayf-socket-endpoint'
  const headersIterator = (await caches.match('./') || {}).headers || {}
  const headers = headersIterator.get(ourHeader) || ''
  _.log(ourHeader + ': ' + headers)
}

main()
export default main