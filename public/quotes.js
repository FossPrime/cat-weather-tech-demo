
const cm = {
  // https://wealthygorilla.com/inspirational-elon-musk-quotes/
  quotes: [
    'Is it worth doing? Will it get us to Mars Sooner, or Later?',
    'It’s OK to have your eggs in one basket as long as you control what happens to that basket.',
    'The first step is to establish that something is possible; then probability will occur.',
    'Some people don’t like change, but you need to embrace change if the alternative is disaster.',
    'I don’t spend my time pontificating about high-concept things; I spend my time solving engineering and manufacturing problems.'
  ]
}

const E_WTF = 'No element with that selector exists.'
const sm = {}
sm.params = (new URL(document.location)).searchParams,
sm.name = !!sm.params.get('quote') // is the string truthy?
sm.selector = '#quote'

const applyQuote = (quoteEl) => {
  const url = new URL(document.URL)
  const quoteParam = !!url.hash.includes('quote=y')
  if (sm.showQuotes || quoteParam) {
    const quoteStr = cm.quotes[Math.ceil(Math.random()*cm.quotes.length-1)]
    quoteEl.innerText = quoteStr
  }
}

function requestFullscreen() {
  document.querySelectorAll('.cat img').forEach((element) => {
    element.addEventListener('click', (event) => {
      element.requestFullscreen()
    })
  })
}

const register = (rawSelector) => {
  requestFullscreen()
  
  sm.selector = rawSelector || sm.selector
  const quoteEl = document.querySelector(sm.selector)
  if (document.querySelector(sm.selector) !== null) {
    applyQuote(quoteEl)
    quoteEl.addEventListener('click', (event) => {
      if (Notification.permission == 'granted') {
        askForBg()
      } else {
        Notification.requestPermission(function(status) {
            console.log('Notification permission status:', status);
        })
      }
    })
    return
  } else {
    throw new Error(E_WTF)
  }
}

async function askForBg () {
  const status = await navigator.permissions.query({
    name: 'periodic-background-sync',
  });
  if (status.state === 'granted') {
    console.log('I love you')
  } else {
    console.log('Mothereffer, what\'s wrong with you?!?!')
  }
}

export const quotes = cm.quotes
export default register