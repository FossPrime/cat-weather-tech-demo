// ISOMORPHIC utils.js, used by server and client
// Careful using with bundlers on this
// - webpack is notorious for making clients look like servers

const pr = new Intl.PluralRules('en-US', {
  type: 'ordinal'
})
const suffixes = new Map([
  ['one',   'st'],
  ['two',   'nd'],
  ['few',   'rd'],
  ['other', 'th'],
]);

export const formatOrdinals = (n) => {
  const rule = pr.select(n);
  const suffix = suffixes.get(rule);
  return `${n}${suffix}`;
};

export const log = (msg, ...rest) => {
  rest.length > 0 && (() => {
    throw new Error('Logger got extra arguments... pass an Array please.')
  })() 
  if (Array.isArray(msg)) {
    
    console.log('=====Start Array Log====')
    msg.forEach((e) => {
      console.log(e)
    })
    console.log('========================')
  } else {
    console.log(msg)
  }
}

// Only use when swallowing an error like situation
export const warn = (msg) => {
  console.log('========WARNING=========')
  console.trace(msg)
  console.log('========================')
}

export default { formatOrdinals, log, warn }